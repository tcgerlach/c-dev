Usage
-------
docker run -it tcgerlach/c-dev

Optionally, include a volume where the C files are that are to be compiled.

Git Repository
--------------
https://bitbucket.org/tcgerlach/c-dev.git

Note
----
This image works with Bitbucket Pipelines to compile C code through a Makefile